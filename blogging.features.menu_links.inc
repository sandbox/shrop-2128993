<?php
/**
 * @file
 * blogging.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function blogging_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: navigation_blog:blog
  $menu_links['navigation_blog:blog'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'blog',
    'router_path' => 'blog',
    'link_title' => 'Blog',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'navigation_blog:blog',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Blog');


  return $menu_links;
}
