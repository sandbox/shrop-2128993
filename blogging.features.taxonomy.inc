<?php
/**
 * @file
 * blogging.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function blogging_taxonomy_default_vocabularies() {
  return array(
    'bog_tags' => array(
      'name' => 'Bog tags',
      'machine_name' => 'bog_tags',
      'description' => 'Category tags for the blog post content type',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
